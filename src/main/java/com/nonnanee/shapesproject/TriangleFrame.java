/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.shapesproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author nonnanee
 */
public class TriangleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(320, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblF = new JLabel("f:", JLabel.TRAILING);
        lblF.setSize(50, 20);
        lblF.setLocation(5, 5);
        lblF.setBackground(Color.WHITE);
        lblF.setOpaque(true);
        frame.add(lblF);
        
         JLabel lblH = new JLabel("h:", JLabel.TRAILING);
        lblH.setSize(50, 20);
        lblH.setLocation(5, 30);
        lblH.setBackground(Color.WHITE);
        lblH.setOpaque(true);
        frame.add(lblH);


        final JTextField txtF = new JTextField();
        txtF.setSize(50, 20);
        txtF.setLocation(60, 5);
        frame.add(txtF);

        final JTextField txtH = new JTextField();
        txtH.setSize(50, 20);
        txtH.setLocation(60, 30);
        frame.add(txtH);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);
        

        JLabel lblResult = new JLabel("Triangle  ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
       
        //Event Driven
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    
                    String strF = txtF.getText();
                    String strH = txtH.getText();
                    
                    double f = Double.parseDouble(strF);// -> NumberFormatException
                    double h = Double.parseDouble(strH);
                    
                    Triangle triangle= new Triangle(f,h);
                    
                    lblResult.setText("Triangle f= "+ String.format("%.2f", triangle.getF()) +" h = "+ String.format("%.2f", triangle.getH())
                            + "area =" + String.format("%.2f", triangle.calArea())
                            + "perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                }catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtF.setText("");
                    txtF.requestFocus();
                     txtH.setText("");
                    txtH.requestFocus();
                }
            } // Anonymous class
        });

        frame.setVisible(true);
    }
}
