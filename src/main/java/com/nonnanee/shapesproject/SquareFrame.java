/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.shapesproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author nonnanee
 */
public class SquareFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Square");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblS = new JLabel("s:", JLabel.TRAILING);
        lblS.setSize(50, 20);
        lblS.setLocation(5, 5);
        lblS.setBackground(Color.WHITE);
        lblS.setOpaque(true);
        frame.add(lblS);

        final JTextField txtS = new JTextField();
        txtS.setSize(50, 20);
        txtS.setLocation(60, 5);
        frame.add(txtS);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Square s= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        //Event Driven
        btnCalculate.addActionListener(new ActionListener() {// Anonymous class
            @Override

            public void actionPerformed(ActionEvent e) {
                try {
//1. ดึงข้อมูล text จาก txtS -> strS
                    String strS = txtS.getText();
// 2. แปลง strS -> s: double parseDouble
                    double s = Double.parseDouble(strS);// -> NumberFormatException
// 3. instance object Square(s) -> square
                    Square square = new Square(s);
//4. update IblResult โดยนำข้อมูลจาก square ไปแสดงให้ครบถ้วน

                    lblResult.setText("Square s = " + String.format("%.2f", square.getS()) 
                            + "area =" + String.format("%.2f", square.calArea()) 
                            + "perimeter = " + String.format("%.2f", square.calPerimeter()));
    }catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtS.setText("");
                    txtS.requestFocus();
                }
            }
        });

        frame.setVisible(true);
    }
}
