/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.shapesproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author nonnanee
 */
public class RectangleFrame {
     public static void main(String[] args) {
        JFrame frame = new JFrame("Rectangle");
        frame.setSize(350, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblW = new JLabel("w:", JLabel.TRAILING);
        lblW.setSize(50, 20);
        lblW.setLocation(5, 5);
        lblW.setBackground(Color.WHITE);
        lblW.setOpaque(true);
        frame.add(lblW);
        
         JLabel lblH = new JLabel("h:", JLabel.TRAILING);
        lblH.setSize(50, 20);
        lblH.setLocation(5, 30);
        lblH.setBackground(Color.WHITE);
        lblH.setOpaque(true);
        frame.add(lblH);


        final JTextField txtW = new JTextField();
        txtW.setSize(50, 20);
        txtW.setLocation(60, 5);
        frame.add(txtW);

        final JTextField txtH = new JTextField();
        txtH.setSize(50, 20);
        txtH.setLocation(60, 30);
        frame.add(txtH);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);
        

        JLabel lblResult = new JLabel("Rectangle  ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(350, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
       
        //Event Driven
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    
                    String strW = txtW.getText();
                    String strH = txtH.getText();
                    
                    double w = Double.parseDouble(strW);// -> NumberFormatException
                    double h = Double.parseDouble(strH);
                    
                 
                    Rectangle rectangle = new Rectangle(w,h);
                    
                    lblResult.setText("Rectangle w= "+ String.format("%.2f", rectangle.getW()) +" h = "+ String.format("%.2f", rectangle.getH())
                            + "area =" + String.format("%.2f", rectangle.calArea())
                            + "perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                }catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtW.setText("");
                    txtW.requestFocus();
                     txtH.setText("");
                    txtH.requestFocus();
                }
            } 
        });

        frame.setVisible(true);
    }
}