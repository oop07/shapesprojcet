/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.shapesproject;

/**
 *
 * @author nonnanee
 */
public class Triangle extends Shape{
    private double f;
    private double h;

    public Triangle(double f, double h) {
        super("Triangle");
        this.f = f;
        this.h = h;
    }

    
    

    public double getF() {
        return f;
    }

    public void setF(double f) {
        this.f = f;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    
    @Override
    public double calArea() {
        return 0.5 * f *h;
    }

    @Override
    public double calPerimeter() {
       return 1/2 * f*h;
    }

    
    
}
